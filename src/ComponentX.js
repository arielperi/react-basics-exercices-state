import React, { Component } from 'react';

class ComponentX extends Component {

  constructor(props) {
    super(props);

    this.state = {
      city: props.city
    }
  }

  onCityChange = (ville) => this.setState({ city: ville })

  render() {
    const { city } = this.state;

    return (
      <div>
        {city}
        <button
          onClick={() => this.onCityChange("JERUSALEM")}
          style={{ display: "block", marginTop: "20px" }}>
          changez la ville pour JLM
        </button>
        <button
          onClick={() => this.onCityChange("TEL AVIV")}
          style={{ display: "block", marginTop: "20px" }}>
          changez la ville pour TLV
        </button>
      </div>
    )
  }
}

export default ComponentX;
