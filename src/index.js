import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ComponentX from './ComponentX';

ReactDOM.render(<ComponentX city="Paris" />, document.getElementById('root'));
